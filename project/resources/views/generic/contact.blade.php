@extends('generic.layout')

<head>
    <title>Contact Us</title>
</head>

<body>
	<div class="pusher">
	<div class="ui vertical stripe segment">
        <div class="ui middle aligned stackable grid container">
            <div class="row">
                <div class="eight wide column right aligned">
                    <i class="big mobile icon"> </i> +880 1743267784
                    <br>
                    <i class="big mail icon"> </i> <a href="samin.chowdhury@northsouth.edu"> samin.chowdhury@northsouth.edu </a>
                    <br>
                    <i class="big building icon"> </i> North South University, Bashundhara, Dhaka-1229, Bangladesh 
                </div>
                <div class="eight wide right floated column">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.0983083934066!2d90.42334931456315!3d23.81510298455756!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c64c103a8093%3A0xd660a4f50365294a!2sNorth+South+University!5e0!3m2!1sen!2sbd!4v1512556628875" width="500" height="480" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>	
</body>

