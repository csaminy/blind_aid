@extends('generic.layout')

<head>
    <title>Newsletter</title>
</head>

<body>
	<div class="pusher">
	<div class="ui vertical stripe" id="setback">
        <div class="ui middle aligned stackable grid container">
            <div class="row">
                <div class="sixteen wide column center aligned">
                    <h1>
                    	Visual Impairments Newsletter Sign-Up
                    </h1>
                    <p>
                    	I am happy to announce that a newsletter is now available. This new format will help you keep up to date. By signing up for this newsletter, you will be the first to learn about new articles and educational opportunities. I am also developing new tools and trainings to support you and you will be the first to know when these are available. But no worries! I promise not to stuff your inbox! And rest assured, I value my privacy as you do. Your email will ALWAYS remain confidential and will NOT be shared!
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="ten wide column">
                    <h2>
                    	Sign Up for the Newsletter to get:
                    </h2>
                    <h4>
                    	<ul>
                    		<li>The latest news</li>
                    		<li>Notification of recent articles</li>
                    		<li>Notification of new job postings</li>
                    		<li>Notification of training opportunities</li>
                    	</ul>
                    </h4>
                    <br>
                    <h2>
                    	Register and get FREE membership:
                    </h2>
                    <h4>
                    	<ul>
                    		<li>Acess to printable forms and templates</li>
                    		<li>Access to the <a href="/viforum"> VI Forum </a> </li>
                    	</ul>
                    </h4>
                </div>

                <div class="four wide column">
                	<div class="ui form">
  						<div class="field">
    						<label> <h4> E-mail </h4> </label>
						    <input type="email" placeholder="joe@schmoe.com">
						</div>
						<div class="ui success message">
							<div class="header">Form Completed</div>
						    <p>You're all signed up for the newsletter.</p>
						</div>
						<div class="ui animated fade secondary submit button" tabindex="0">
  							<div class="visible content">Subscribe</div>
  							<div class="hidden content">
    							<i class="large Newspaper icon"> </i>
  							</div>
						</div>
						
					</div>
                </div>
            </div>

        </div>
    </div>
</div>	
</body>

