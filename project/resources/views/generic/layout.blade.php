<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{mix('css/app.css')}}"/>
</head>
<body>


<!-- Page Contents -->
<div class="pusher">
    <div class="ui vertical masthead center aligned">
        <div class="ui grid">
            <div class="ui container fluid">
                <div class="ui fixed inverted menu stackable">
                    <a class="item" href="/">
                       <i class="big home icon"></i>
                    </a>
                    
                    <a>
                        <div class="ui simple dropdown item">
                            VI Jobs      
                            <i class="dropdown icon"></i>
                            <div class="menu stackable">
                                <a class="item"><i class="fa fa-users"></i> Post A Job </a>
                                <a class="item"><i class="fa fa-user"></i> Find Jobs </a>
                                <a class="item"><i class="fa fa-user"></i> Submit CV </a>
                            </div>
                        </div>
                    </a>

                    <a>
                    <div class="ui simple dropdown item">
                        Programs     
                        <i class="dropdown icon"></i>
                        <div class="menu stackable">
                            <div class="item">
                                <i class="dropdown icon"></i>
                                <span class="text"> Vision Professionals </span>      
                                <div class="menu stackable">
                                    <a class="item"><i class="fa fa-users"></i> Become a Vision Professional </a>
                                    <a class="item"><i class="fa fa-user"></i> Professional Preparation Programs </a>
                                    <a class="item"><i class="fa fa-user"></i> Teacher of Students with Visual Impairments </a>
                                </div>
                            </div>

                            <div class="item">
                                <i class="dropdown icon"></i>
                                <span class="text"> V.I. Books Resources </span>      
                                <div class="menu stackable">
                                    <a class="item"><i class="fa fa-users"></i> VI Resource Books </a>
                                    <a class="item"><i class="fa fa-user"></i> Braille Books Resources </a>
                                </div>
                            </div>

                            <div class="item">
                                <i class="dropdown icon"></i>
                                <span class="text"> Program Resources </span>      
                                <div class="menu stackable">
                                    <a class="item"><i class="fa fa-users"></i> VI Organizations </a>
                                    <a class="item"><i class="fa fa-user"></i> Braille Resources </a>
                                    <a class="item"><i class="fa fa-user"></i> Schools for the Blinds </a>
                                </div>
                            </div>
                           
                            <div class="item">
                                <i class="dropdown icon"></i>
                                <span class="text"> Parents Resources </span>      
                                <div class="menu stackable">
                                    <a class="item"><i class="fa fa-users"></i> Online Parent Resources </a>
                                    <a class="item"><i class="fa fa-user"></i> Grief and Suffering Books </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>

                    <a>
                    <div class="ui simple dropdown item">
                        Service     
                        <i class="dropdown icon"></i>
                        <div class="menu stackable">
                            <a class="item"><i class="fa fa-user"></i> Referrals </a>
                            <div class="item">
                                    <i class="dropdown icon"></i>
                                    <span class="text"> Vision Exams </span>      
                                    <div class="menu stackable">
                                        <a class="item"><i class="fa fa-users"></i> Vision Test and Tools </a>
                                        <a class="item"><i class="fa fa-user"></i> Visual Acuity </a>
                                    </div>
                                </div>
                           
                            
                            <a class="item"><i class="fa fa-user"></i> Other Evaluations </a>
                        </div>
                    </div>
                    </a>

                    <a>
                        <div class="ui simple dropdown item">
                            Adaptations      
                            <i class="dropdown icon"></i>
                            <div class="menu stackable">
                                <a class="item"><i class="fa fa-users"></i> Unique Visual Needs </a>
                                <a class="item"><i class="fa fa-user"></i> Instructional Adaptations </a>
                                <a class="item"><i class="fa fa-user"></i> Environmental Adaptations </a>
                            </div>
                        </div>
                    </a>

                    <a class="item" href="/#aboutus">About</a>
                    <div class="right item">
                        <a class="ui button" href="/login">Log in</a>
                        <a class="ui button" href="/register">Sign Up</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="ui inverted vertical footer segment">
    <div class="ui container">
        <div class="ui stackable inverted divided equal height stackable grid">
            
            <div class="three wide column center aligned">
                <a href="/"> <h4 class="ui inverted header">  Home </h4> </a>
                <div class="ui inverted link list">
                    <a href="/sitemap" class="item">Sitemap</a>
                    <a href="/#aboutus" class="item">About</a>
                    <a href="/newsletter" class="item">Newsletter</a>
                    <a href="/viforum" class="item">VI Forum</a>
                    <a href="/useragreement" class="item">User Agreement</a>
                </div>
            </div>
            
            <div class="three wide column center aligned">
                <h4 class="ui inverted header">VI Programs</h4>
                <div class="ui inverted link list">
                    <a href="#" class="item">Vision Professionals</a>
                    <a href="#" class="item">VI Book Resources</a>
                    <a href="#" class="item">VI Program Resources</a>
                    <a href="#" class="item">VI Parent Resources</a>
                </div>
            </div>

            <div class="three wide column center aligned">
                <h4 class="ui inverted header">Adaptations</h4>
                <div class="ui inverted link list">
                    <a href="#" class="item">Unique Visual Needs</a>
                    <a href="#" class="item">Instructional Adaptations</a>
                    <a href="#" class="item">Environmental Adaptations</a>
                </div>
            </div>
            

            <div class="three wide column center aligned" id="contact">
                <h4 class="ui inverted header">Contact Us</h4>
                <p>HS<sub>2</sub>  Factory</p>
                <p>©Loo-Creative</p>
                <i class="mobile icon"> </i> +880 1743267784
                <br>
            </div>        
            <div class="four wide column center aligned">
                <button class="ui facebook button">
                  <i class="facebook icon"></i>
                  Facebook
                </button>
                <br> <br>
                <button class="ui twitter button">
                  <i class="twitter icon"></i>
                  Twitter
                </button>
                <br> <br>
                <button class="ui youtube button">
                    <i class="youtube icon"></i>
                    YouTube
                </button>
            </div>
        </div>
    </div>
</div>
<script src="{{mix('js/app.js')}}"></script>
</body>
</html>
